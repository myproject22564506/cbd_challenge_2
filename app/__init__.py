from flask import Flask
from dotenv import load_dotenv
import os 

load_dotenv()


app = Flask(__name__)

# Configure session to use filesystem
app.config["SESSION_PERMANENT"] = True
app.config["SESSION_TYPE"] = "filesystem"
Session(app)

# Dummy user and product data
users = {"user1": "password"}

products = {
    1: {"name": "Laptop", "price": 1000},
    2: {"name": "Mouse", "price": 50},
    3: {"name": "Keyboard", "price": 100}
}

from app import routes
